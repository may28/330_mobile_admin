### 架构说明：
  
前端： vue 2.5.2 + vue-ydui + webpack  
UI框架ydui： http://vue.ydui.org/docs/#/quickstart  
引入插件请查看 package.json  

源文件路径：/resources  
生产路径： /public  
注意提交生产环境前要执行压缩： npm run production  
