import Vue from 'vue';
import VueRouter from 'vue-router';
import Resource from 'vue-resource';
import YDUI from 'vue-ydui';
import 'vue-ydui/dist/ydui.rem.css';
import 'vue-ydui/dist/ydui.base.css';

import App from './app.vue';

Vue.use(VueRouter);
Vue.use(Resource);
Vue.use(YDUI);

Vue.component('main-nav', require('./pages/includes/mainnav.vue'));
Vue.component('sub-nav', require('./pages/includes/subnav.vue'));
Vue.component('page-title', require('./pages/includes/pagetitle.vue'));
Vue.component('add-file', require('./pages/includes/addfile.vue'));
Vue.component('music-item', require('./pages/includes/musicitem.vue'));

import Index from './pages/index.vue';
import Login from './pages/login.vue';
import Home from './pages/home.vue';
import Music from './pages/music.vue';
import Musicadd from './pages/musicadd.vue';
import Musicedit from './pages/musicedit.vue';
import Album from './pages/album.vue';
import Albumadd from './pages/albumadd.vue';
import Albumedit from './pages/albumedit.vue';
import Video from './pages/video.vue';
import Project from './pages/project.vue';
import Crowdfund from './pages/crowdfund.vue';
import Product from './pages/product.vue';
import Profile from './pages/profile.vue';
import Wallet from './pages/wallet.vue';
import Admire from './pages/admire.vue';
import Order from './pages/order.vue';
import Authentication from './pages/authentication.vue';
import Account from './pages/account.vue';
import Sites from './pages/sites.vue';

document.addEventListener('DOMContentLoaded', function () {
    typeof FastClick === 'function' && FastClick.attach(document.body);
}, false);

const router = new VueRouter({
    routes: [
        {path: '/login', name: 'login', component: Login},
        {path: '/home', name: 'home', component: Home},
        {path: '/', name: 'index', component: Index},
        {path: '/music', name: 'music',  component: Music},
        {path: '/musicadd', name: 'musicadd',  component: Musicadd},
        {path: '/musicedit', name: 'musicedit',  component: Musicedit},
        {path: '/album', name: 'album',  component: Album},
        {path: '/albumadd', name: 'albumadd',  component: Albumadd},
        {path: '/albumedit', name: 'albumedit',  component: Albumedit},
        {path: '/video', name: 'video',  component: Video},
        {path: '/project', name: 'project',  component: Project},
        {path: '/crowdfund', name: 'crowdfund',  component: Crowdfund},
        {path: '/product', name: 'product',  component: Product},
        {path: '/profile', name: 'profile',  component: Profile},
        {path: '/wallet', name: 'wallet',  component: Wallet},
        {path: '/admire', name: 'admire',  component: Admire},
        {path: '/order', name: 'order',  component: Order},
        {path: '/authentication', name: 'authentication',  component: Authentication},
        {path: '/account', name: 'account',  component: Account},
        {path: '/sites', name: 'sites',  component: Sites}
    ]
});

const app = new Vue({
    router: router,
    render: v => v(App),
}).$mount('#app');

let scrollTop = 0;

router.beforeEach((route, redirect, next) => {
    document.title = '330 - ' + route.name;
    next();
});

router.afterEach(route => {

});

// document.addEventListener('touchmove', this._preventDefault, { passive: false });

let mobileApi = function (route, params)
{
    var url = window.location.origin + "/api/v1/mobile/" + route;
    if (undefined !== params) {
        for (var key in params) {
            url += url.indexOf("?") < 0? "?" : "&";
            url += key + "=" + params[key];
        }
    }
    return url;
};

let adminApi = function (route, params)
{
    return mobileApi("/" + window.userid + "/" + route, params);
};


/**
 * mobile get ajax
 * @param url 请求地址
 * @param then 请求成功回调函数
 */
let mobileGet = function (url, then)
{
    axios.get(url).then(then).catch(function (error) {
        if (error.response.status == 401) {
            location.href = window.loginUrl;
        }
        console.log(error);
    });
};

/**
 * mobile post ajax
 * @param url 请求地址
 * @param params 请求参数
 * @param successCallback 请求执行成功回调函数
 * @param failCallback 请求执行失败回调函数
 * @param errorCallback 请求错误回调函数
 */
let mobilePost = function (url, params, successCallback, failCallback, errorCallback)
{
    if (undefined === successCallback || 'function' !== (typeof successCallback)) {
        successCallback = function () {};
    }
    if (undefined === failCallback || 'function' !== (typeof failCallback)) {
        failCallback = function () {};
    }
    if (undefined === errorCallback || 'function' !== (typeof errorCallback)) {
        errorCallback = function () {};
    }
    axios.post(url, params).then(function (response) {
        if (0 == response.data.errcode) {
            successCallback(response);
        } else {
            failCallback(response);
        }
    }).catch(function (error) {
        if (error.response.status == 401) {
            location.href = window.loginUrl;
        }
        if (error.response.status == 422) {
            if (typeof error.response.data == 'object') {
                let message = error.response.data;
                for (let key in message) {
                    if (typeof message[key] == 'array') {
                        window.mogilePostMessage(key, message[key][0]);
                    } else {
                        window.mobilePostMessage(key, message[key]);
                    }
                    break;
                }
            }
        }
        console.log(error);
        errorCallback(error);
    });
};

/**
 * mobile put ajax
 * @param url 请求地址
 * @param params 请求参数
 * @param successCallback 请求执行成功回调函数
 * @param failCallback 请求执行失败回调函数
 * @param errorCallback 请求错误回调函数
 */
let mobilePut = function (url, params, successCallback, failCallback, errorCallback)
{
    params._method = "put";
    mobilePost(url, params, successCallback, failCallback, errorCallback);
};

/**
 * mobile delete ajax
 * @param url 请求地址
 * @param params 请求参数
 * @param successCallback 请求执行成功回调函数
 * @param failCallback 请求执行失败回调函数
 * @param errorCallback 请求错误回调函数
 */
let mobileDelete = function (url, params, successCallback, failCallback, errorCallback)
{
    params._method = "delete";
    mobilePost(url, params, successCallback, failCallback, errorCallback);
};

// dialog loading
let submitStart = function (app, msg)
{
    if (undefined === msg) {
        msg = "操作中";
    }
    app.$dialog.loading.open(msg);
};

let submitSuccess = function (app, msg)
{
    if (undefined === msg) {
        msg = '操作成功';
    }
    app.$dialog.loading.close();
    app.$dialog.toast({
        mes: msg,
        icon: 'success',
        timeout: 1500
    });
};

let submitFail = function (app, msg)
{
    if (undefined === msg) {
        msg = '操作失败';
    }
    app.$dialog.loading.close();
    app.$dialog.toast({
        mes: msg,
        icon: 'error',
        timeout: 1500
    });
};

let mobilePostMessage = function (key, message)
{
    if (undefined === window.app) {
        alert(message);
    } else {
        window.submitFail(window.app, message);
    }
};

window.mobileGet = mobileGet;
window.mobilePost = mobilePost;
window.mobilePut = mobilePut;
window.mobileDelete = mobileDelete;
window.submitStart = submitStart;
window.submitSuccess = submitSuccess;
window.submitFail = submitFail;
window.mobilePostMessage = mobilePostMessage;
